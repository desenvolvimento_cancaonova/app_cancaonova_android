package com.cancaonova.model;

/**
 * Created by desenvolvimento on 19/07/17.
 */

public class About {

    private String Title;
    private String Description;


    public About (String title){
        this.setTitle(title);
    }

    public About (String title, String description){
        this.setTitle(title);
        this.setDescription(description);
    }


    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
