package com.cancaonova.activity;


import android.support.design.internal.NavigationMenu;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.cancaonova.DAO.ConfigDAO;
import com.cancaonova.DAO.LikeDAO;
import com.cancaonova.DAO.ReaderDAO;
import com.cancaonova.R;
import com.cancaonova.lib.couch.model.Post;
import com.cancaonova.util.Actions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;

public class WebContentActivity extends AppCompatActivity {
    ProgressBar loading;
    Post post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Bundle extras = getIntent().getExtras();
        String url = extras.getString("url");

        String post_json = extras.getString("post");

        if (post_json != "") {
            post = new Gson().fromJson(post_json, Post.class);
        }

        loading = (ProgressBar) findViewById(R.id.loading);
        final WebView webView = (WebView) findViewById(R.id.web_view);
        webView.setWebViewClient(new WebViewClient() {

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                loading.setVisibility(View.GONE);
                webView.loadUrl("file:///android_asset/offline.html");
                post = null;
             }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                loading.setVisibility(View.GONE);
                post = null;
                webView.loadUrl("file:///android_asset/offline.html");
            }

            public void onPageFinished(WebView view, String url) {
                loading.setVisibility(View.GONE);
                configMenu();
                ConfigDAO configDAO = new ConfigDAO(getApplicationContext(), null);
                String urlFrenteMissao = configDAO.getUrlFrenteMissao();
                configDAO.closeConnection();
                if(post==null && !url.equals("file:///android_asset/offline.html") && !url.equals("https://www.cancaonova.com/politica-de-privacidade/?readMode=true") && !url.equals(urlFrenteMissao)){
                    webView.loadUrl("file:///android_asset/offline.html");
                }
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                webView.loadUrl("file:///android_asset/offline.html");
            }
        });

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        firebaseAnalytics();
        getSupportActionBar().setTitle(getString(R.string.screen_title_voltar));

    }

    private void configMenu(){
        FabSpeedDial fabSpeedDial = (FabSpeedDial) findViewById(R.id.web_menu);

        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {

                MenuItem readerMenu = navigationMenu.findItem(R.id.menu_reader);
                MenuItem likeMenu = navigationMenu.findItem(R.id.menu_like);

                ReaderDAO readerDAO = new ReaderDAO(getApplicationContext());
                if (readerDAO.exist(post.get_id()) && readerMenu != null) {
                    readerMenu.setIcon(R.drawable.ic_reader_full_over);
                }
                readerDAO.closeConnection();
                readerDAO = null;

                LikeDAO likeDAO = new LikeDAO(getApplicationContext());
                if (likeDAO.exist(post.get_id()) && likeMenu != null) {
                    likeMenu.setIcon(R.drawable.ic_like_over);
                }
                likeDAO.closeConnection();
                likeDAO = null;

                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {


                if (menuItem.getItemId() == R.id.menu_share) {
                    Actions.share(WebContentActivity.this, post);
                } else if (menuItem.getItemId() == R.id.menu_reader) {
                    Actions.reader(WebContentActivity.this, post);
                } else if (menuItem.getItemId() == R.id.menu_like) {

                    String like = "like";

                    if (post.getLike()) {
                        like = "dislike";
                        post.setLike(false);
                    } else {
                        post.setLike(true);
                    }

                    Actions.like(like, WebContentActivity.this, post, null, 0);
                }
                return false;
            }
        });

        if (post == null) {
            fabSpeedDial.setVisibility(View.GONE);
        }

    }

    private void firebaseAnalytics() {
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString("screen_name","WebContentActivity");

        if(post != null) {
            bundle.putString("url", post.getLink());
            bundle.putString("title", post.getTitle());
        }
        mFirebaseAnalytics.logEvent("screenview",bundle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;

    }

}
