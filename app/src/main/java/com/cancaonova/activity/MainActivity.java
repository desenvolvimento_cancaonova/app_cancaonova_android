package com.cancaonova.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.cancaonova.DAO.BaseDAO;
import com.cancaonova.DAO.ConfigDAO;
import com.cancaonova.DAO.ReaderDAO;
import com.cancaonova.R;
import com.cancaonova.fragment.AboutFragment;
import com.cancaonova.fragment.LikeFragment;
import com.cancaonova.fragment.PrayerFragment;
import com.cancaonova.fragment.ReaderFragment;
import com.cancaonova.fragment.TimelineFragment;
import com.cancaonova.lib.loginunico.interfaces.LoginUnicoListener;
import com.cancaonova.lib.loginunico.view.LoginView;
import com.couchbase.lite.replicator.Replication;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.onesignal.OneSignal;

import java.net.MalformedURLException;
import java.net.URL;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, DrawerLayout.DrawerListener, BaseDAO.CouchDataBaseListener, LoginUnicoListener {

    TextView txtBadge;
    NavigationView navigationView;
    Replication puller;
    LoginView loginView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        configMenu();

        OneSignal.startInit(this).init();
        Fabric.with(this, new Crashlytics());
        firebaseAnalytics();

        changeFragment(new TimelineFragment());

    }

    @Override
    protected void onResume() {
        super.onResume();
        createPullerConfigs();
    }

    private void firebaseAnalytics() {
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString("screen_name", "MainActivity");
        mFirebaseAnalytics.logEvent("screenview", bundle);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
        boolean changeButtonStatus = true;

        Fragment fragment = null;
        if (id == R.id.nav_timeline) {
            fragment = new TimelineFragment();
            getSupportActionBar().setTitle(getString(R.string.screen_title_timeline));
        } else if (id == R.id.nav_like) {
            fragment = new LikeFragment();
            getSupportActionBar().setTitle(getString(R.string.screen_title_like));
        } else if (id == R.id.nav_reader) {
            fragment = new ReaderFragment();
            getSupportActionBar().setTitle(getString(R.string.screen_title_reader));
        } else if (id == R.id.nav_prayer) {

/** todo validação para forçar o usuario a efetuar o login
 User user = com.cancaonova.lib.loginunico.Actions.loggedUser(MainActivity.this);

 if (user == null) {
 Message.showMessageLogin(MainActivity.this, "Pedido de Oração");
 changeButtonStatus = false;

 } else {
 */
            fragment = new PrayerFragment();
            getSupportActionBar().setTitle(getString(R.string.screen_title_prayer));

            /*          } */


        } else if (id == R.id.nav_location) {
            ConfigDAO configDAO = new ConfigDAO(getApplicationContext(), this);
            String url = configDAO.getUrlFrenteMissao();
            configDAO.closeConnection();

            Intent intent = new Intent(this, WebContentActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("url", url);
            intent.putExtra("showbutton", false);
            startActivity(intent);

        } else if (id == R.id.nav_shopping) {

            ConfigDAO configDAO = new ConfigDAO(getApplicationContext(), this);
            String url = configDAO.getUrlShopping();
            configDAO.closeConnection();

            openUrl(url);
            changeButtonStatus = false;
        } else if (id == R.id.nav_about) {
            fragment = new AboutFragment();
            getSupportActionBar().setTitle(getString(R.string.screen_title_about));
        }

        if (changeButtonStatus) {
            for (int i = 0; i < navigationView.getMenu().size(); i++) {
                navigationView.getMenu().getItem(i).setChecked(false);
            }

            item.setChecked(true);
            changeFragment(fragment);

            closeMenu();

        }


        return true;
    }


    private void configMenu() {
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        toggle.setDrawerIndicatorEnabled(false);
        drawer.setDrawerListener(this);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerLayout = navigationView.getHeaderView(0);
        loginView = headerLayout.findViewById(R.id.loginView);
        loginView.setLoginUnicoListener(MainActivity.this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                if (!drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        setupBadge();

    }


    private void closeMenu() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void openUrl(String url) {
        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(myIntent);
        closeMenu();
    }


    private void changeFragment(Fragment fragment) {

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, fragment);
            fragmentTransaction.commit();
        }
    }


    private void createPullerConfigs() {

        if (puller != null) {
            puller.start();
        } else {

            URL url = null;
            try {
                url = new URL(getString(R.string.url_replication_config));
            } catch (MalformedURLException e) {
                url = null;
            }


            ConfigDAO configDAO = new ConfigDAO(getApplicationContext(), this);
            if (url != null) {
                puller = configDAO.createPuller(url);
            }
        }
    }



    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        if (txtBadge == null) {
            setupBadge();
        }
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        hideKeyboard();
        setupBadge();
    }

    private void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }catch (Exception e){

        }
    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    @Override
    public void onReplicationDone() {
    }

    @Override
    public void onReplicationError() {
    }

    @Override
    public void onReplication() {

    }


    private void getTxtBadge() {
        if (navigationView != null && txtBadge == null) {
            MenuItem menuItem = navigationView.getMenu().findItem(R.id.nav_reader);
            if (menuItem != null) {
                View actionView = MenuItemCompat.getActionView(menuItem);
                txtBadge = actionView.findViewById(R.id.txt_badge);
            }
        }
    }

    private void setupBadge() {

        getTxtBadge();

        if (txtBadge != null) {

            ReaderDAO readerDAO = new ReaderDAO(getApplicationContext());
            int readers = readerDAO.count();
            readerDAO.closeConnection();
            String values = "";
            if (readers > 0) {
                if (readers > 99) {
                    values = "99+";
                } else {
                    values = String.valueOf(readers);
                }
                txtBadge.setText(values);
                txtBadge.setVisibility(View.VISIBLE);
            } else {
                txtBadge.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onLogoutListener() {
        changeFragment(new TimelineFragment());
        getSupportActionBar().setTitle(getString(R.string.screen_title_timeline));
    }

    @Override
    public void onLoginListener() {

    }
}
