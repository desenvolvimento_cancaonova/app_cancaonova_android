package com.cancaonova.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.cancaonova.DAO.ConfigDAO;
import com.cancaonova.R;
import com.cancaonova.util.Message;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SuggestionActivity extends AppCompatActivity {

    private EditText editTextEmail, editTextSugestao;
    private Button btEnviarSugestao;

    ProgressDialog loading;

    @Override
    protected void onStop() {
        super.onStop();

        SharedPreferences settings = getSharedPreferences(getString(R.string.shared_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("email", editTextEmail.getText().toString());
        editor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestion);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        editTextEmail = (EditText) findViewById(R.id.email_text);
        editTextSugestao = (EditText) findViewById(R.id.suggestion_text);
        btEnviarSugestao = (Button) findViewById(R.id.bt_send_suggestion);

        btEnviarSugestao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editTextSugestao.getText().toString().trim().length() == 0) {
                    Message.show(SuggestionActivity.this, getString(R.string.suggestion_title), getString(R.string.suggestion_empty));
                } else {
                    sendMail();
                }

            }
        });

        getSupportActionBar().setTitle(getString(R.string.screen_title_suggestion));


        loadPrefs();

        firebaseAnalytics();

    }

    private void firebaseAnalytics() {
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString("screen_name", "SuggestionActivity");
        mFirebaseAnalytics.logEvent("screenview", bundle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    private void loadPrefs() {


            SharedPreferences settings = getSharedPreferences(getString(R.string.shared_key), Context.MODE_PRIVATE);

            String email = settings.getString("email", "");
            editTextEmail.setText(email);

    }

    private void sendMail() {
        loading = ProgressDialog.show(SuggestionActivity.this, "", "Enviando", true);
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        OkHttpClient client = new OkHttpClient();

        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("subject", getString(R.string.suggestion_from))
                .add("email", editTextEmail.getText().toString())
                .add("suggestion", editTextSugestao.getText().toString());


        RequestBody formBody = formBuilder.build();

        ConfigDAO configDAO = new ConfigDAO(getApplicationContext(), null);
        String url = configDAO.getUrlSendMail();
        configDAO.closeConnection();

        Request request = new Request.Builder().url( url ).post(formBody).build();


        client.newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(final Call call, IOException e) {
                        loading.dismiss();

                        Message.show(SuggestionActivity.this, getString(R.string.suggestion_title), getString(R.string.suggestion_error));
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        String res = response.body().string();
                        loading.dismiss();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                editTextSugestao.setText("");
                            }
                        });

                        Message.show(SuggestionActivity.this, getString(R.string.suggestion_title), getString(R.string.suggestion_ok));
                    }
                });

    }


}
