package com.cancaonova.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cancaonova.R;


/**
 * Created by desenvolvimento on 03/08/17.
 */

public class LikeViewHolder   extends RecyclerView.ViewHolder {

    private TextView txtTime;
    private TextView txtCanal;
    private TextView txtDescricao;
    private TextView txtTitulo;
    private ImageView imageView;
    private ImageView btLike;
    private ImageView btReader;
    private ImageView btShare;
    private View tapView;


    public LikeViewHolder(View itemView) {
        super(itemView);


        setTxtTime((TextView) itemView.findViewById(R.id.txt_time));
        setTxtCanal((TextView) itemView.findViewById(R.id.txt_channel));
        setTxtDescricao((TextView) itemView.findViewById(R.id.txt_description));
        setTxtTitulo((TextView) itemView.findViewById(R.id.txt_title));
        setImageView((ImageView) itemView.findViewById(R.id.img_thumb));
        setBtLike((ImageView) itemView.findViewById(R.id.bt_like));
        setBtReader((ImageView) itemView.findViewById(R.id.bt_reader));
        setBtShare((ImageView)itemView.findViewById(R.id.bt_share));

        setTapView(itemView.findViewById(R.id.tap_view));

    }

    public TextView getTxtTime() {
        return txtTime;
    }

    public void setTxtTime(TextView txtTime) {
        this.txtTime = txtTime;
    }

    public TextView getTxtCanal() {
        return txtCanal;
    }

    public void setTxtCanal(TextView txtCanal) {
        this.txtCanal = txtCanal;
    }

    public TextView getTxtDescricao() {
        return txtDescricao;
    }

    public void setTxtDescricao(TextView txtDescricao) {
        this.txtDescricao = txtDescricao;
    }

    public TextView getTxtTitulo() {
        return txtTitulo;
    }

    public void setTxtTitulo(TextView txtTitulo) {
        this.txtTitulo = txtTitulo;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public ImageView getBtLike() {
        return btLike;
    }

    public void setBtLike(ImageView btLike) {
        this.btLike = btLike;
    }

    public ImageView getBtReader() {
        return btReader;
    }

    public void setBtReader(ImageView btReader) {
        this.btReader = btReader;
    }

    public ImageView getBtShare() {
        return btShare;
    }

    public void setBtShare(ImageView btShare) {
        this.btShare = btShare;
    }


    public View getTapView() {
        return tapView;
    }

    public void setTapView(View tapView) {
        this.tapView = tapView;
    }

}
