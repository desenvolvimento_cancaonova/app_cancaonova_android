package com.cancaonova.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cancaonova.R;


public class ViewHolder {
    //int position = 0;

    private TextView txtTime;
    private TextView txtCanal;
    private TextView txtDescricao;
    private TextView txtTitulo;
    private TextView txtLike;
    private ImageView imageView;
    private ImageView btLike;
    private ImageView btReader;
    private ImageView btShare;

    private View tapView;
    View view;
    private int type;
/*
    Post post;
    Activity activity;

    String channel = "";
    String title = "";
    String description = "";
    Boolean fullUnitTime = false;*/

    public ViewHolder(View view){
        this.view = view;
        findElements();
    }
   /* public ViewHolder(Activity activity, Post post) {
        //  this.position = position;
        this.post = post;
        this.activity = activity;


        populate();
        createView();
    }
*/

    private void findElements(){
        setTxtTime((TextView) this.view.findViewById(R.id.txt_time));
        setTxtCanal((TextView) this.view.findViewById(R.id.txt_channel));
        setTxtDescricao((TextView) this.view.findViewById(R.id.txt_description));
        setTxtTitulo((TextView) this.view.findViewById(R.id.txt_title));
        setTxtLike((TextView) this.view.findViewById(R.id.txt_like));
        setImageView((ImageView) this.view.findViewById(R.id.img_thumb));
        setTapView(this.view.findViewById(R.id.tap_view));
        setBtLike((ImageView) this.view.findViewById(R.id.bt_like));
        setBtReader((ImageView) this.view.findViewById(R.id.bt_reader));
        setBtShare((ImageView)this.view.findViewById(R.id.bt_share));

        boolean isTablet = this.view.getResources().getBoolean(R.bool.isTablet);


        if (isTablet) {
            getTxtDescricao().setVisibility(View.VISIBLE);
        } else {
            getTxtDescricao().setVisibility(View.GONE);
        }


    }
  /*  private void populate() {
        channel = post.getSource();
        title = post.getTitle();
        description = post.getContent();
        fullUnitTime = false;
    }

    private void createView() {
        View convertView = null;
        LayoutInflater mInflater = LayoutInflater.from(this.activity);
        if (post.getWidget().equals("loja")) {
            convertView = mInflater.inflate(R.layout.item_timeline_shopping, null);
        } else if (post.getWidget().equals("docao")) {
            convertView = mInflater.inflate(R.layout.item_timeline_donate, null);
            description = this.activity.getString(R.string.donate_month) + " " + post.getTitle();
            title = post.getContent() + "%";
            CircleProgressView donateCircleView = (CircleProgressView) convertView.findViewById(R.id.item_donate_circle_view);
            donateCircleView.setValueAnimated(Float.parseFloat(post.getContent()), 1000);
            fullUnitTime = true;
        } else {
            convertView = mInflater.inflate(R.layout.item_timeline_default, null);
        }

        this.view = convertView;
        findElements();


        getTxtCanal().setText(channel);
        getTxtDescricao().setText(description);
        getTxtTitulo().setText(title);



        // tapView.setTag(position);
        getTapView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actions.actionView(activity, post);
            }
        });


        ImageView btShare = (ImageView) convertView.findViewById(R.id.bt_share);

        btShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actions.share(activity, post);
            }
        });



        getBtLike().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actions.like(activity, post);
            }
        });



        if (getBtReader() != null) {

            getBtReader().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Actions.reader(activity, post);
                }
            });
        }


        new Thread(new Runnable() {
            public void run() {

                LikeDAO likeDAO = new LikeDAO(activity.getApplicationContext());
                if (likeDAO.exist(post.getId())) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            getBtLike().setImageResource(R.drawable.ic_like_over);
                        }});
                }

                likeDAO.closeConnection();
                likeDAO = null;

                ReaderDAO readerDAO = new ReaderDAO(activity.getApplicationContext());
                if (readerDAO.exist(post.getId())) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            getBtReader().setImageResource(R.drawable.ic_reader_over);
                        }});
                }
                readerDAO.closeConnection();
                readerDAO = null;
            }
        }).start();


        this.view = convertView;

    }

*/
    public View getView() {
        return this.view;
    }

    public TextView getTxtTime() {
        return txtTime;
    }

    public void setTxtTime(TextView txtTime) {
        this.txtTime = txtTime;
    }

    public TextView getTxtCanal() {
        return txtCanal;
    }

    public void setTxtCanal(TextView txtCanal) {
        this.txtCanal = txtCanal;
    }

    public TextView getTxtDescricao() {
        return txtDescricao;
    }

    public void setTxtDescricao(TextView txtDescricao) {
        this.txtDescricao = txtDescricao;
    }

    public TextView getTxtTitulo() {
        return txtTitulo;
    }

    public void setTxtTitulo(TextView txtTitulo) {
        this.txtTitulo = txtTitulo;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public ImageView getBtLike() {
        return btLike;
    }

    public void setBtLike(ImageView btLike) {
        this.btLike = btLike;
    }

    public ImageView getBtReader() {
        return btReader;
    }

    public void setBtReader(ImageView btReader) {
        this.btReader = btReader;
    }

    public View getTapView() {
        return tapView;
    }

    public void setTapView(View tapView) {
        this.tapView = tapView;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ImageView getBtShare() {
        return btShare;
    }

    public void setBtShare(ImageView btShare) {
        this.btShare = btShare;
    }

    public TextView getTxtLike() {
        return txtLike;
    }

    public void setTxtLike(TextView txtLike) {
        this.txtLike = txtLike;
    }
}
