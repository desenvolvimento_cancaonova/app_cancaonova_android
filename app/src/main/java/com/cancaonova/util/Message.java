package com.cancaonova.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.cancaonova.activity.MainActivity;
import com.cancaonova.lib.loginunico.*;

/**
 * Created by desenvolvimento on 21/07/17.
 */
public class Message {

    public static void show(final Activity activity, final String title, final String message){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                AlertDialog.Builder dialog = new AlertDialog.Builder(activity)
                        .setTitle(title)
                        .setMessage(message);

                final AlertDialog alert = dialog.create();

                Window window = alert.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();

                wlp.gravity = Gravity.BOTTOM;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);

                alert.show();
                final Handler handler  = new Handler();
                final Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        if (alert.isShowing()) {
                            alert.dismiss();
                        }
                    }
                };

                handler.postDelayed(runnable, 2500);
            }
        });
    }

    public static  void showMessageLogin(final Activity activity, final String  title){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(activity)
                        .setTitle(title)
                        .setMessage("Para enviar\n"+ title + "\né necessário estar logado.\n\n Gostaria de efetuar login?")
                        .setCancelable(false)
                        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                com.cancaonova.lib.loginunico.Actions.openLoginPage(activity);
                            }
                        }).show();

            }
        });
    }
}
