package com.cancaonova.util;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.cancaonova.DAO.ConfigDAO;
import com.cancaonova.DAO.LikeDAO;
import com.cancaonova.DAO.ReaderDAO;
import com.cancaonova.R;
import com.cancaonova.activity.WebContentActivity;
import com.cancaonova.adapter.AdapterBase;
import com.cancaonova.lib.couch.model.Post;
import com.google.gson.Gson;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

//TODO rever a ideia dessa classe

public class Actions {


    public static void actionView(Activity activity, Post post) {

        if (post.getWidget().equals("post")) {

            Intent intent = new Intent(activity, WebContentActivity.class);
            intent.putExtra("url", appInUrl(post.getAction()[0].getAndroid()));
            intent.putExtra("showbutton", true);

            intent.putExtra("post", new Gson().toJson(post));

            activity.startActivity(intent);


        } else if (post.getWidget().equals("loja") || post.getWidget().equals("doacao")) {

            if (post.getAction()[0].getAndroid().length() > 1) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(appInUrl(post.getAction()[0].getAndroid())));
                activity.startActivity(intent);
            }

        } else if (post.getWidget().equals("url_externa")){
            if (post.getAction()[0].getAndroid().length() > 1) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(post.getAction()[0].getAndroid()));
                activity.startActivity(intent);
            }
        } else if (post.getWidget().equals("app")) {

            Intent intent = activity.getPackageManager().getLaunchIntentForPackage(post.getFirstAction());

            if (intent != null) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            } else {
                intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("market://details?id=" + post.getFirstAction()));

                activity.startActivity(intent);
            }
        }

    }

    public static void share(Activity activity, Post post) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, post.getTitle());
        sendIntent.putExtra(Intent.EXTRA_TEXT, post.getShareText());
        sendIntent.setType("text/plain");
        activity.startActivity(sendIntent);
    }

    private static String appInUrl(String url) {
        if (!url.contains("?")) {
            url += "?app=cn&readMode=true";
        } else {
            url += "&app=cn&readMode=true";
        }
        return url;
    }


    public static boolean reader(Activity activity, Post post) {
        ReaderDAO readerDAO = new ReaderDAO(activity.getApplicationContext());
        Boolean b = readerDAO.insertOrDelete(post.get_id());
        readerDAO.closeConnection();
        return b;
    }

    //TODO rever essa assinatura que esta muito feia
    public static void like(final String type, final Activity activity, final Post post, final AdapterBase.AdapterListener actionListener, final int position) {

        java.util.Date date = new java.util.Date();
        Timestamp ts_now = new Timestamp(date.getTime());

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(5000, TimeUnit.MILLISECONDS)
                .writeTimeout(5000, TimeUnit.MILLISECONDS)
                .readTimeout(5000, TimeUnit.MILLISECONDS)
                .build();

        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("type", type)
                .add("link", post.getLink())
                .add("date", ts_now.toString())
                .add("id", post.getId())
                .add("origin", "appcn");

        RequestBody formBody = formBuilder.build();

        ConfigDAO configDAO = new ConfigDAO(activity.getApplicationContext(), null);
        String url = configDAO.getUrlSendLike();
        configDAO.closeConnection();

        Request request = new Request.Builder().url(url).post(formBody).build();

        client.newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(final Call call, IOException e) {
                        messageError(activity, actionListener, position);
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {

                        if (response.code() == 200 || response.code() == 201) {
                            String res = response.body().string();
                            LikeDAO likeDAO = new LikeDAO(activity.getApplicationContext());
                            likeDAO.insertOrDelete(post.get_id());
                            likeDAO.closeConnection();
                        } else {
                            messageError(activity, actionListener, position);
                        }
                    }
                });
    }


    private static void messageError(Activity activity, AdapterBase.AdapterListener actionListener, int position) {
        Message.show(activity, activity.getString(R.string.like_title), activity.getString(R.string.like_send_error));

        if (actionListener != null) {
            actionListener.onItemLikeChange(position);
        }
    }

    public static String timeOfOccurrence(Activity activity, Date date, boolean fullUnitTime) {
        String timeUnit = "min.";
        if (fullUnitTime) {
            timeUnit = activity.getString(R.string.minute);
        } else {
            timeUnit = activity.getString(R.string.minute_small);
        }

        String time = "";
        Date date2 = new Date();

        long finalBuildTime = Utils.getDateDiff(date, date2, TimeUnit.MINUTES);

        if (finalBuildTime > 60) {
            timeUnit = activity.getString(R.string.hour);
            finalBuildTime = Utils.getDateDiff(date, date2, TimeUnit.HOURS);

            if (finalBuildTime > 24) {
                timeUnit = activity.getString(R.string.day);
                finalBuildTime = Utils.getDateDiff(date, date2, TimeUnit.DAYS);
            }

            if (finalBuildTime > 1) {
                timeUnit = String.format("%s%s", timeUnit, activity.getString(R.string.basic_plural));
            }

           /* if (fullUnitTime) {
                time =  String.format("%s %s %s", activity.getString(R.string.ha), finalBuildTime, timeUnit);
            } else {*/
            time = String.format("%s %s %s", activity.getString(R.string.ha), finalBuildTime, timeUnit);
            // }

        } else if (finalBuildTime < 1) {
            time = activity.getString(R.string.less_minute);
        } else {

            if (fullUnitTime) {
                if (finalBuildTime > 1) {
                    timeUnit = time = String.format("%s%s", timeUnit, activity.getString(R.string.basic_plural));
                }

                time = String.format("%s %s %s %s", activity.getString(R.string.updated), activity.getString(R.string.ha), finalBuildTime, timeUnit);
            } else {
                time = String.format("%s %s %s", activity.getString(R.string.ha), finalBuildTime, timeUnit);
            }

        }

        return time;
    }


    public static String likeFromPost(Activity activity, Post post) {

        String retorno = "";
        if (post.getAdditional_data() != null) {
            int likes = post.getAdditional_data().getTotal_likes_int();
            DecimalFormat df = new DecimalFormat("#,###");

            if (likes > 0) {
                if (likes > 1) {
                    retorno = String.format("%s %s", df.format(likes), activity.getString(R.string.like_plural));
                } else {
                    retorno = String.format("%s %s", df.format(likes), activity.getString(R.string.like_singular));
                }
            }
        }
        return retorno;
    }
}
