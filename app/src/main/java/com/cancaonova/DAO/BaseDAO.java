package com.cancaonova.DAO;

import android.content.Context;

import com.couchbase.lite.Emitter;
import com.couchbase.lite.Mapper;
import com.couchbase.lite.View;

import java.util.Map;

/**
 * Created by desenvolvimento on 21/12/2017.
 */

public class BaseDAO extends com.cancaonova.lib.couch.DAO.BaseDAO {
    public BaseDAO(Context context, String dbName) {
        super(context, dbName);
    }

    @Override
    protected void createViews() {
        super.createViews();

         View view_url_send_like = database.getView("url_send_like");
        view_url_send_like.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                if ("url_send_like".equals(document.get("source_slug"))) {
                    emitter.emit(document.get("id"), null);
                }
            }
        }, "1");
        View view_url_shopping = database.getView("url_shopping");
        view_url_shopping.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                if ("url_shopping".equals(document.get("source_slug"))) {
                    emitter.emit(document.get("id"), null);
                }
            }
        }, "1");

        View view_prayer_to = database.getView("prayer_to");
        view_prayer_to.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                if ("prayer_to".equals(document.get("source_slug"))) {
                    emitter.emit(document.get("id"), null);
                }
            }
        }, "1");


        View view2 = database.getView("timeline");
        view2.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                emitter.emit(document.get("date"), null );
            }
        }, "4");

        View view3 = database.getView("archive");
        view3.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                emitter.emit(document.get("date"), null );
            }
        }, "3");

        View view4 = database.getView("fixed");
        view4.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                emitter.emit(document.get("id"), null);
            }
        }, "1");

        View view5 = database.getView("reader");
        view5.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                emitter.emit(document.get("date"), null );
            }
        }, "1");


        View view6 = database.getView("like");
        view6.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                emitter.emit(document.get("date"), null );
            }
        }, "1");
    }
}
