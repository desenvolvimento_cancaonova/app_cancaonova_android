package com.cancaonova.DAO;

import android.content.Context;
import android.util.Log;

import com.cancaonova.lib.couch.model.Post;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.replicator.Replication;
import com.google.gson.Gson;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class FixedDAO extends BaseDAO {

    public FixedDAO(Context context, CouchDataBaseListener couchDataBaseListener) {
        super(context, "fixed");
        this.CBLListener = couchDataBaseListener;
    }

    public Post getFixed() {
        QueryEnumerator result = super.getResults("fixed", null, 1);
        Post post = null;
        if (result.getCount() > 0) {

            for (Iterator<QueryRow> it = result; it.hasNext(); ) {
                QueryRow row = it.next();
                Gson gson = new Gson();
                Object movieObj = row.getDocument().getProperties();
                String jsonString = gson.toJson(movieObj, Map.class);

                try {
                    post = new Post();
                    post = gson.fromJson(jsonString, Post.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return post;

    }

    public Replication createPuller(URL replicationURL) {

        Replication puller = database.createPullReplication(replicationURL);
        puller.setContinuous(true);

        List<String> channels = new ArrayList<>();
        channels.add("fixed");
        puller.setChannels(channels);

        puller.setAuthenticator(this.auth);

        puller.addChangeListener(new Replication.ChangeListener() {
            @Override
            public void changed(Replication.ChangeEvent event) {

                Replication replication = event.getSource();
                if (replication.getStatus() == Replication.ReplicationStatus.REPLICATION_OFFLINE || replication.getStatus() == Replication.ReplicationStatus.REPLICATION_IDLE) {
                    if (CBLListener != null) {
                        CBLListener.onReplicationDone();
                    }
                } else if (event.getError() != null) {
                    if (CBLListener != null) {
                        CBLListener.onReplicationError();
                    }
                }
            }
        });
        puller.start();

        return puller;
    }
}