package com.cancaonova.DAO;

import android.content.Context;
import android.util.Log;

import com.cancaonova.lib.couch.model.Post;
import com.couchbase.lite.Document;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.replicator.Replication;
import com.google.gson.Gson;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class TimelineDAO extends BaseDAO {

    final String VIEW = "timeline";
    ReaderDAO readerDAO;
    LikeDAO likeDAO;


    public TimelineDAO(Context context) {
        super(context, "timeline");
    }

    public TimelineDAO(Context context, CouchDataBaseListener couchDataBaseListener) {
        super(context, "timeline");
        this.CBLListener = couchDataBaseListener;
    }


    public Replication createPuller(URL replicationURL) {

        Replication puller = database.createPullReplication(replicationURL);
        puller.setContinuous(true);

        List<String> channels = new ArrayList<>();
        channels.add("timeline");
        puller.setChannels(channels);

        puller.setAuthenticator(this.auth);


        puller.addChangeListener(new Replication.ChangeListener() {

            @Override
            public void changed(Replication.ChangeEvent event) {

                Replication replication = event.getSource();

                if (replication.getStatus() == Replication.ReplicationStatus.REPLICATION_OFFLINE) {
                    if (CBLListener != null) {
                        CBLListener.onReplicationDone();
                    }
                } else if (replication.getStatus() == Replication.ReplicationStatus.REPLICATION_IDLE) {
                    if (replication.getChangesCount() > 0) {
                        if (replication.getChangesCount() == replication.getCompletedChangesCount()) {
                            if (CBLListener != null) {
                                CBLListener.onReplicationDone();
                            }
                        }
                    }
                } else if (event.getError() != null) {
                    if (CBLListener != null) {
                        CBLListener.onReplicationError();
                    }
                }

            }
        });
        puller.start();

        return puller;

    }

    public int countResult() {
        int count = 0;

        QueryEnumerator result = super.getResults(VIEW);

        if (result != null) {
            count = result.getCount();
        }

        return count;

    }

    public ArrayList<Post> getTimeline(Object starKey, int limit) {

        if (this.readerDAO == null) {
            this.readerDAO = new ReaderDAO(this.context);
        }

        if (this.likeDAO == null) {
            this.likeDAO = new LikeDAO(this.context);
        }


        QueryEnumerator result = super.getResults(VIEW, starKey, limit);

        ArrayList<Post> posts = new ArrayList<Post>();
        if (result != null) {
            for (Iterator<QueryRow> it = result; it.hasNext(); ) {
                QueryRow row = it.next();


                Post p = toPost(row.getDocument().getProperties());

                if (p != null) {
                    p.setKey(row.getKey());
                    posts.add(p);
                }

            }
        }
        return posts;
    }

    //TODO rever esse metodo para coloca-lo no base
    public Post get(String documentId) {

        if (this.readerDAO != null) {
            this.readerDAO.closeConnection();
        }
        this.readerDAO = new ReaderDAO(this.context);

        if (this.likeDAO != null) {
            this.likeDAO.closeConnection();
        }
        this.likeDAO = new LikeDAO(this.context);

        Document document = database.getDocument(documentId);

        return toPost(document.getProperties());
    }

    //TODO rever esse metodo para coloca-lo no base
    private Post toPost(Object obj) {

        Post p = null;
        Gson gson = new Gson();
        String jsonString = gson.toJson(obj, Map.class);

        try {
            p = gson.fromJson(jsonString, Post.class);
            if (p != null) {
                p.setReader(readerDAO.exist(p.get_id()));
                p.setLike(likeDAO.exist(p.get_id()));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return p;
    }
}
