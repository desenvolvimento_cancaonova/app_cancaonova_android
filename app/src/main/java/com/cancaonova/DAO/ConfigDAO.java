package com.cancaonova.DAO;

import android.content.Context;

import com.cancaonova.R;
import com.cancaonova.lib.campanha.model.Donate;
import com.cancaonova.DAO.BaseDAO;
import com.cancaonova.lib.couch.model.Post;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.replicator.Replication;
import com.google.gson.Gson;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class ConfigDAO extends BaseDAO {

    public ConfigDAO(Context context, CouchDataBaseListener couchDataBaseListener) {
        super(context, "config");
        this.CBLListener = couchDataBaseListener;
    }


    public Replication createPuller(URL replicationURL) {

        Replication puller = database.createPullReplication(replicationURL);
        puller.setContinuous(false);

        List<String> channels = new ArrayList<>();
        channels.add("config");
        puller.setChannels(channels);

        puller.setAuthenticator(this.auth);

        puller.addChangeListener(new Replication.ChangeListener() {
            @Override
            public void changed(Replication.ChangeEvent event) {

                Replication replication = event.getSource();
 

                if (replication.getStatus() == Replication.ReplicationStatus.REPLICATION_OFFLINE) {
                    if (CBLListener != null) {
                        CBLListener.onReplicationDone();
                    }
                } else if (replication.getStatus() == Replication.ReplicationStatus.REPLICATION_IDLE) {

                    if (CBLListener != null) {
                        CBLListener.onReplicationDone();
                    }


                } else if (event.getError() != null) {
                    if (CBLListener != null) {
                        CBLListener.onReplicationError();
                    }
                }


            }
        });
        puller.start();

        return puller;

    }

    public Donate getDonate() {

        Donate donate = null;
        QueryEnumerator result = this.getResults("donate");

        for (Iterator<QueryRow> it = result; it.hasNext(); ) {
            QueryRow row = it.next();

            Gson gson = new Gson();
            try {
                Object obj = row.getDocument().getProperties();
                String jsonString = gson.toJson(obj, Map.class);
                donate = gson.fromJson(jsonString, Donate.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return donate;
    }





    public String getUrlTerms() {

        String url = null;

        if(url == null) {
            url = context.getString(R.string.url_terms);
        }

        return url;
    }
    public String getMailPrayer() {

        String url = getUrl ("prayer_to");

        if(url == null) {
            url = context.getString(R.string.prayer_to);
        }

        return url;
    }
   /* public String getUrlDonate() {

        String url = getUrl ("url_donate");

        if(url == null) {
            url = context.getString(R.string.url_donate);
        }

        return url;
    }*/
    public String getUrlSendLike() {

        String url = getUrl ("url_send_like");

        if(url == null) {
            url = context.getString(R.string.url_send_like);
        }

        return url;
    }
    public String getUrlShopping() {

        String url = getUrl ("url_shopping");

        if(url == null) {
            url = context.getString(R.string.url_shopping);
        }

        return url;
    }
    public String getUrlSendMail(){
        String url = getUrl ("url_send_mail");

        if(url == null) {
            url = context.getString(R.string.url_send_mail);
        }

        return url;
    }

    private String getUrl(String view){
        QueryEnumerator result = this.getResults(view);
        String url = null;
        for (Iterator<QueryRow> it = result; it.hasNext(); ) {
            QueryRow row = it.next();

            Gson gson = new Gson();
            try {
                Object obj = row.getDocument().getProperties();
                String jsonString = gson.toJson(obj, Map.class);
                Post p  = gson.fromJson(jsonString, Post.class);
                url = p.getContent();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return url;
    }

    public String getUrlFrenteMissao() {
        return getUrl("frentes-de-missao");
    }
}
