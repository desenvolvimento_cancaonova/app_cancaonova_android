package com.cancaonova.DAO;

import android.content.Context;

import com.cancaonova.lib.couch.model.Post;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;

import java.util.ArrayList;
import java.util.Iterator;

public class ReaderDAO extends BaseDAO {
    final String VIEW = "reader";

    public ReaderDAO(Context context) {
        super(context, "reader");
    }

    public ArrayList<Post> getReader( ) {

        TimelineDAO timelineDAO = new TimelineDAO(this.context);

        QueryEnumerator result = super.getResults(VIEW);

        ArrayList<Post> posts = new ArrayList<Post>();
        if (result != null) {
            for (Iterator<QueryRow> it = result; it.hasNext(); ) {
                QueryRow row = it.next();

                Post p = timelineDAO.get(row.getDocumentId());

                if(p != null){
                    p.setKey(row.getKey().toString());
                    posts.add(p);
                }
            }
        }

        timelineDAO.closeConnection();;

        return posts;
    }


    public int count(){
        QueryEnumerator result = super.getResults(VIEW);

        return result.getCount();

    }
}