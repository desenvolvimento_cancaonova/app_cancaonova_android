package com.cancaonova.adapter;


import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cancaonova.R;
import com.cancaonova.lib.couch.model.Post;
import com.cancaonova.holder.ViewHolder;
import com.cancaonova.util.Actions;

import java.util.ArrayList;


public class AdapterBase extends BaseAdapter {

    private ArrayList<Post> listPosts = new ArrayList<Post>();
    protected Activity activity;
    protected AdapterListener adapterListener;
    LayoutInflater mInflater;
    boolean fullUnitTime = false;

    int[] layout = new int[3];

    public AdapterBase(Activity activity, ArrayList<Post> listPosts, AdapterListener adapterListener) {

        this.setListPosts(listPosts);
        this.activity = activity;
        this.adapterListener = adapterListener;
        this.mInflater = LayoutInflater.from(this.activity);

    }


    @Override
    public int getCount() {
        return getListPosts().size();
    }

    @Override
    public Object getItem(int position) {
        return getListPosts().get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;

        if (getListPosts().get(position).getWidget().equals("loja")) {
            type = 1;
        } else if (getListPosts().get(position).getWidget().equals("doacao")) {
            type = 2;
        }
        return type;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = getView(position, convertView);
        final View finalConvertView = convertView;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                populateData(position, finalConvertView);
            }
        });

        executeListenerDisplay(position);

        return convertView;

    }

    public int getLayout(int viewType) {
        int layout;
        if (viewType == 1) {
            layout = R.layout.item_timeline_shopping;
        } else if (viewType == 2) {
            layout = R.layout.item_timeline_donate;
        } else {
            layout = R.layout.item_timeline_default;
        }
        return layout;
    }

    private View getView(int position, View convertView) {

        ViewHolder holder = null;
        int viewType = this.getItemViewType(position);

        convertView = mInflater.inflate(getLayout(viewType), null);

        if (holder == null) {
            holder = new ViewHolder(convertView);
        }

        holder.setType(viewType);
        convertView.setTag(holder);

        return convertView;

    }

    public void populateData(int position, View convertView) {

        ViewHolder holder = (ViewHolder) convertView.getTag();

        if (holder != null) {
            //todo aqui deveria converter para donate
            Post post = getListPosts().get(position);
            String description = post.getContent();
            String title = post.getTitle();
            fullUnitTime = false;
            if (holder.getType() == 2) {
                //this.activity.getString(R.string.donate_month) + " " +
                description = post.getTitle();
                title = post.getContent() + "%";
                holder.getTxtDescricao().setVisibility(View.VISIBLE);
                fullUnitTime = true;
            }

            holder.getTxtCanal().setText(post.getSource());
            holder.getTxtDescricao().setText(description);
            holder.getTxtTitulo().setText(title);

            if (holder.getImageView() != null && post.getImage_url().getThumbnail() != null) {
                Glide.with(this.activity).load(post.getImage_url().getThumbnail()).into(holder.getImageView());
            }

            if (holder.getTxtTime() != null) {
                holder.getTxtTime().setText(Actions.timeOfOccurrence(this.activity, post.getDate(), fullUnitTime));
            }

            if (holder.getBtReader() != null) {

                if (post.getReader()) {
                    holder.getBtReader().setImageResource(R.drawable.ic_reader_over);
                } else {
                    holder.getBtReader().setImageResource(R.drawable.ic_reader);
                }

                holder.getBtReader().setTag(position);
                holder.getBtReader().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Actions.reader(activity, getListPosts().get((int) v.getTag()));
                        executeListenerData(v);
                    }
                });

            }

            if (holder.getBtLike() != null) {
                if (post.getLike()) {
                    holder.getBtLike().setImageResource(R.drawable.ic_like_over);
                } else {
                    holder.getBtLike().setImageResource(R.drawable.ic_like);
                }

                if (holder.getTxtLike() != null) {
                    holder.getTxtLike().setVisibility(View.INVISIBLE);
                    holder.getTxtLike().setText(Actions.likeFromPost(this.activity, post));
                    if( holder.getTxtLike().getText().length() > 0){
                        holder.getTxtLike().setVisibility(View.VISIBLE);
                    }
                    holder.getBtLike().setTag(R.id.TXTLIKE, holder.getTxtLike() );
                }

                holder.getBtLike().setTag(R.id.INDEXLIST, position);

                holder.getBtLike().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int position = (int) v.getTag(R.id.INDEXLIST);
                        Post p = getListPosts().get(position);
                        ImageView img = (ImageView) v;
                        String like = "like";
                        TextView txtLike = (TextView) v.getTag(R.id.TXTLIKE);

                        int total_likes = p.getAdditional_data().getTotal_likes_int();

                        if (p.getLike()) {
                            img.setImageResource(R.drawable.ic_like);
                            like = "dislike";
                            p.setLike(false);
                            total_likes--;

                        } else {
                            img.setImageResource(R.drawable.ic_like_over);
                            p.setLike(true);
                            total_likes++;
                        }

                        p.getAdditional_data().setTotal_likes(String.valueOf(total_likes));

                        //TODO MELHOR ESSE CODIGO
                        if(txtLike != null){
                            String l = Actions.likeFromPost(activity, p);
                            if( l.length() > 0){
                                txtLike.setVisibility(View.VISIBLE);
                            }else {
                                txtLike.setVisibility(View.INVISIBLE);
                            }
                            txtLike.setText(l);
                        }

                        Actions.like(like , activity, p , adapterListener, position);


                    }
                });

            }

            if (holder.getBtShare() != null) {
                holder.getBtShare().setTag(position);
                holder.getBtShare().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Actions.share(activity, getListPosts().get((int) v.getTag()));
                    }
                });
            }
            holder.getTapView().setTag(position);
            holder.getTapView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Actions.actionView(activity, getListPosts().get((int) v.getTag()));
                    executeListenerOpen(v);
                }
            });
        }

        // convertView.setTag(holder);
    }

    public ArrayList<Post> getListPosts() {
        return listPosts;
    }

    public void setListPosts(ArrayList<Post> listPosts) {
        this.listPosts = listPosts;
    }


    private void executeListenerOpen(View v) {
        if (adapterListener != null) {
            adapterListener.onItemOpen((int) v.getTag());
        }
    }

    protected void executeListenerData(View v) {
        if (adapterListener != null) {
            adapterListener.onItemDataChange((int) v.getTag());
        }
    }

    private void executeListenerDisplay(int position) {
        if (this.adapterListener != null) {
            this.adapterListener.onItemDisplayChange(position);
        }
    }

    //todo preciso pensar em um local ou forma melhor para usar o itemLikeChange
    public interface AdapterListener {
        void onItemDisplayChange(int position);

        void onItemDataChange(int position);

        void onItemOpen(int position);

        void onItemLikeChange(int position);
    }


}