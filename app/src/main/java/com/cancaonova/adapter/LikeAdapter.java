package com.cancaonova.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cancaonova.R;
import com.cancaonova.lib.couch.model.Post;
import com.cancaonova.util.Actions;

import java.util.ArrayList;


public class LikeAdapter extends AdapterBase {


    public LikeAdapter(Activity activity, ArrayList<Post> listPosts, AdapterListener adapterListener) {
        super(activity, listPosts, adapterListener);
    }

    @Override
    public int getLayout(int viewType) {

        int layout =  R.layout.item_like_default;

        if (viewType == 2) {
            layout = R.layout.item_like_donate;
        }

        return layout;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = super.getView(position, convertView, parent);


        ImageView btClose = (ImageView) convertView.findViewById(R.id.bt_close);

        btClose.setTag(position);
        btClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actions.like("dislike", activity, getListPosts().get((int) v.getTag()), adapterListener,  (int) v.getTag());
                executeListenerData(v);
            }
        });

        return convertView;
    }
}