package com.cancaonova.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.cancaonova.DAO.ConfigDAO;
import com.cancaonova.R;
import com.cancaonova.activity.SuggestionActivity;
import com.cancaonova.activity.WebContentActivity;
import com.cancaonova.lib.loginunico.model.User;
import com.cancaonova.model.About;
import com.cancaonova.util.Message;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

import java.util.ArrayList;

/**
 * Created by desenvolvimento on 19/07/17.
 */

public class AboutAdapter extends BaseAdapter {

    Activity activity;
    ArrayList<About> data;
    private static LayoutInflater inflater = null;

    public AboutAdapter(Activity activity, ArrayList<About> data) {
        this.activity = activity;
        this.data = data;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View vi = inflater.inflate(android.R.layout.simple_list_item_1, null);
        TextView text;
        Switch switch_notification = null;

        if (position == 4) {
            vi = inflater.inflate(R.layout.item_about_notification, null);
            TextView text2 = (TextView) vi.findViewById(R.id.text2);
            text2.setText(data.get(position).getDescription());
            text = (TextView) vi.findViewById(R.id.text1);

            switch_notification = (Switch) vi.findViewById(R.id.switch_notification);
            OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
            switch_notification.setChecked(status.getSubscriptionStatus().getSubscribed());

            switch_notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    OneSignal.setSubscription(isChecked == true);
                }
            });

        } else {
            text = (TextView) vi.findViewById(android.R.id.text1);
        }


        vi.setMinimumHeight(activity.getResources().getInteger(R.integer.list_about_height));

        if (Build.VERSION.SDK_INT < 23) {
            text.setTextColor(activity.getResources().getColor(R.color.aboutItemNormal));
        } else {
            text.setTextColor(activity.getColor(R.color.aboutItemNormal));
        }

        text.setText(data.get(position).getTitle());

        vi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (position) {
                    case 0:
                        rateApp();
                        break;
                    case 1:
                        suggestions();
                        break;
                    case 2:
                        terms();
                        break;
                    case 3:
                        callGooglePlayDeveloperApps();
                        break;
                }
            }
        });

        return vi;
    }



    private void terms(){

        ConfigDAO configDAO = new ConfigDAO(activity.getApplicationContext(), null);
        String url = configDAO.getUrlTerms();
        configDAO.closeConnection();

        Intent intent = new Intent(activity, WebContentActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("url", url);
        intent.putExtra("showbutton", false);
        activity.startActivity(intent);
    }
    private void suggestions() {
/** todo validação para forçar o usuario a efetuar o login
        User user = com.cancaonova.lib.loginunico.Actions.loggedUser(activity);

        if (user == null) {
            Message.showMessageLogin(activity, "Sugestão");
        }else {*/
            Intent intent = new Intent(activity, SuggestionActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
     /*   }*/
    }

    private void rateApp() {

        final String appPackageName = activity.getPackageName();
        Uri url;
        try {
           url = Uri.parse("market://details?id=" + appPackageName);
        } catch (android.content.ActivityNotFoundException anfe) {
            url = Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName);
        }

        openExternalURL(url);
    }

    private void callGooglePlayDeveloperApps(){

        Uri url;
        try {
            url = Uri.parse("market://search?q=pub:Canção+Nova");
        } catch (android.content.ActivityNotFoundException anfe) {
            url = Uri.parse("http://play.google.com/store/apps/developer?id=Canção+Nova");
        }

        openExternalURL(url);

    }

    private void openExternalURL(Uri url){

        Intent intent = new Intent(Intent.ACTION_VIEW, url);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        activity.startActivity(intent);

    }
}