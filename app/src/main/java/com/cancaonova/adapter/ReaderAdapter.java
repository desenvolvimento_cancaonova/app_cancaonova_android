package com.cancaonova.adapter;


import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cancaonova.R;
import com.cancaonova.lib.couch.model.Post;
import com.cancaonova.util.Actions;

import java.util.ArrayList;

public class ReaderAdapter extends AdapterBase {


    public ReaderAdapter(Activity activity, ArrayList<Post> listPosts, AdapterListener adapterListener) {
        super(activity, listPosts, adapterListener);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.item_reader_default;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = super.getView(position, convertView, parent);


        ImageView btClose = (ImageView) convertView.findViewById(R.id.bt_close);

        btClose.setTag(position);
        btClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actions.reader(activity, getListPosts().get((int) v.getTag()));
                executeListenerData(v);
            }
        });

        return convertView;
    }
}
