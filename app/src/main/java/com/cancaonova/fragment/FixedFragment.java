package com.cancaonova.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cancaonova.DAO.BaseDAO;
import com.cancaonova.DAO.FixedDAO;
import com.cancaonova.R;
import com.cancaonova.lib.couch.model.Post;
import com.cancaonova.util.Actions;
import com.couchbase.lite.replicator.Replication;

import java.net.MalformedURLException;
import java.net.URL;

public class FixedFragment extends Fragment implements BaseDAO.CouchDataBaseListener {

    View view;
    Replication puller;
    Post fixed_post;
    ImageView imgThumb;
    TextView txtChannel;
    TextView txtTitle;
    TextView txtDescription;


    @Override
    public void onStop() {
        super.onStop();
        if(puller!=null) {
            puller.stop();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(puller!=null) {
            puller.stop();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.view = inflater.inflate(R.layout.fragment_timeline_fixed, container, false);

        this.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actions.actionView(getActivity(), fixed_post);
            }
        });

        this.view.setTranslationY(0 - view.getHeight());

        imgThumb = (ImageView) this.view.findViewById(R.id.img_thumb);
        txtChannel = (TextView) this.view.findViewById(R.id.txt_channel);
        txtTitle = (TextView) this.view.findViewById(R.id.txt_title);
        txtDescription = (TextView) this.view.findViewById(R.id.txt_description);

        boolean isTablet = this.view.getResources().getBoolean(R.bool.isTablet);

        if(isTablet){
            txtDescription.setVisibility(View.VISIBLE);
        }else{
            txtDescription.setVisibility(View.GONE);
        }

        getFixed();
        createPuller();
        return this.view;
    }

    public void getFixed() {

        FixedDAO fixedDAO = new FixedDAO(getActivity().getApplicationContext(), this);
        fixed_post = fixedDAO.getFixed();

        if( getActivity()!= null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (fixed_post != null) {
                        txtChannel.setText(fixed_post.getSource());
                        txtTitle.setText(fixed_post.getTitle());
                        txtDescription.setText(fixed_post.getContent());
                        if(fixed_post.getImage_url() != null){
                            Glide.with(getActivity()).load(fixed_post.getImage_url().getThumbnail()).into(imgThumb);
                        }
                        if(view.getVisibility() != View.VISIBLE) {
                            view.setVisibility(View.VISIBLE);
                            view.setAlpha(0.0f);
                            view.animate().setDuration(200)
                                    .translationY(0)
                                    .alpha(1.0f)
                                    .setListener(null);

                        }

                    } else {
                        if(view.getVisibility() == View.VISIBLE) {
                            view.animate().setDuration(200)
                                    .translationY(0 - view.getHeight())
                                    .alpha(0.0f)
                                    .setListener(new AnimatorListenerAdapter() {
                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            super.onAnimationEnd(animation);
                                            view.setVisibility(View.GONE);
                                        }
                                    });
                        }
                    }
                }
            });
        }
    }

    private void createPuller() {

        URL url = null;
        try {
            url = new URL(getString(R.string.url_replication_timeline_fixed));
        } catch (MalformedURLException e) {
            url = null;
        }

        FixedDAO fixedDAO = new FixedDAO(getActivity(), this);
        if (url != null) {
            puller = fixedDAO.createPuller(url);
        }
    }

    @Override
    public void onReplicationDone() {
        getFixed();
    }

    @Override
    public void onReplicationError() {
    }

    @Override
    public void onReplication() {

    }
}
