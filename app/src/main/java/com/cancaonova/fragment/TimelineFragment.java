package com.cancaonova.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.cancaonova.DAO.BaseDAO;
import com.cancaonova.DAO.TimelineDAO;
import com.cancaonova.R;
import com.cancaonova.adapter.AdapterBase;
import com.cancaonova.adapter.TimeLineAdapter;
import com.cancaonova.lib.couch.model.Post;
import com.couchbase.lite.replicator.Replication;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class TimelineFragment extends Fragment implements BaseDAO.CouchDataBaseListener, AdapterBase.AdapterListener {

    private View view;
    private ListView listView;
    private Replication puller;
    private ArrayList<Post> posts = new ArrayList<Post>();
    private TimeLineAdapter listAdapter;
    private Boolean loadResult = false;
    private TimelineDAO timelineDAO;
    private FloatingActionButton btUpdates;
    private ProgressBar loading;
    private int openPosition = -1;
    private int oldCount = 0;


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (puller != null) {
            puller.stop();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (puller != null) {
            puller.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (puller != null) {
            puller.start();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity().getApplicationContext() != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isVisible()) {
                        createPuller();
                        getTimeline();
                    }
                }
            }, 1000);
        } else {
            getActivity().recreate();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_timeline, container, false);
        listView = this.view.findViewById(R.id.list_timeline);
        loading = this.view.findViewById(R.id.loading);

        btUpdates = this.view.findViewById(R.id.bt_updates);
        btUpdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loading.setVisibility(View.VISIBLE);
                Animation animation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.rotate);
                btUpdates.startAnimation(animation);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        posts = new ArrayList<Post>();
                        getTimeline();
                        listView.smoothScrollToPosition(0);
                    }
                }, 300);


            }
        });


        return this.view;
    }

    private void createPuller() {

        URL url = null;
        try {
            url = new URL(getString(R.string.url_replication_timeline));
        } catch (MalformedURLException e) {
            url = null;
        }

        if (timelineDAO == null) {
            timelineDAO = new TimelineDAO(getActivity().getApplicationContext(), TimelineFragment.this);
        }

        if (url != null) {
            if (puller != null) {
                puller.stop();
            }

            puller = timelineDAO.createPuller(url);
        }

    }

    public void getTimeline() {

        Object lastKey = null;
        if (posts.size() > 0) {
            lastKey = posts.get(posts.size() - 1).getKey();
        }

        oldCount = timelineDAO.countResult();

        ArrayList<Post> temp_posts = timelineDAO.getTimeline(lastKey, 200);

        if (temp_posts.size() > 0) {
            posts.addAll(temp_posts);

            if (listAdapter == null) {
                listAdapter = new TimeLineAdapter(getActivity(), posts, TimelineFragment.this);
                listView.setAdapter(listAdapter);
                listView.setSelection(0);
            } else {
                listAdapter.setListPosts(posts);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listAdapter.notifyDataSetChanged();
                    }
                });
            }
            loadResult = false;

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loading.setVisibility(View.GONE);
                    btUpdates.setAnimation(null);
                    btUpdates.setVisibility(View.INVISIBLE);
                }
            });
        }

    }

    @Override
    public void onReplicationDone() {
        Log.e("MDK", "timeline  onReplicationDone");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (oldCount == 0) {
                    getTimeline();
                } else if (oldCount < timelineDAO.countResult()) {
                    btUpdates.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onReplicationError() {
        Log.e("MDK", "timeline onReplicationError");
    }

    @Override
    public void onReplication() {
        Log.e("MDK", "timeline  onReplication");
    }

    @Override
    public void onItemDisplayChange(int position) {
    }

    @Override
    public void onItemDataChange(int position) {
        itemDataChange(position);
    }

    @Override
    public void onItemOpen(int position) {
        this.openPosition = position;
    }

    @Override
    public void onItemLikeChange(final int position) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    itemDataChange(position);
                }
            });
        }
    }

    private void itemDataChange(int position) {
        View convertView = listView.getChildAt(position - listView.getFirstVisiblePosition());

        if (convertView == null) {
            return;
        }

        Post p = timelineDAO.get(posts.get(position).get_id());
        if (p != null) {
            listAdapter.getListPosts().set(position, p);
            listAdapter.populateData(position, convertView);
        }
    }

}
