package com.cancaonova.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.cancaonova.DAO.ConfigDAO;
import com.cancaonova.R;

import com.cancaonova.lib.loginunico.model.User;
import com.cancaonova.util.Message;


import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class PrayerFragment extends Fragment {

    View view;

    EditText editName;
    EditText editEmail;
    EditText editPrayer;
    Button btSend;
    ProgressDialog loading;


    @Override
    public void onStop() {
        super.onStop();

        SharedPreferences settings = getActivity().getSharedPreferences(getString(R.string.shared_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("email", editEmail.getText().toString());
        editor.putString("name", editName.getText().toString());

        editor.commit();


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.view = inflater.inflate(R.layout.fragment_prayer, container, false);

        editName = (EditText) this.view.findViewById(R.id.name_text);
        editEmail = (EditText) this.view.findViewById(R.id.email_text);
        editPrayer = (EditText) this.view.findViewById(R.id.prayer_text);
        btSend = (Button) this.view.findViewById(R.id.bt_send_prayer);

        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editPrayer.getText().toString().trim().length() == 0) {
                    Message.show(getActivity(), getString(R.string.prayer_title), getString(R.string.prayer_empty));
                } else {
                    sendMail();
                }
            }
        });


        loadPrefs();

        return this.view;
    }

    @Override
    public void onResume() {
        super.onResume();
       loadPrefs();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void loadPrefs() {
      SharedPreferences settings = getActivity().getSharedPreferences(getString(R.string.shared_key), Context.MODE_PRIVATE);

       /*
       String name = settings.getString("name", "");
       String email = settings.getString("email", "");
        */
    /*
    todo validação para forçar o usuario a efetuar o login
        User user = com.cancaonova.lib.loginunico.Actions.loggedUser(getContext());

        if (user == null) {
            blockScreen(false);
        }else{
            blockScreen(true);
            editName.setText(user.getNome());
            editEmail.setText(user.getEmail());
        }
     */

        blockScreen(true);
    }


    private void blockScreen(Boolean b){
        editPrayer.setEnabled(b);
        editName.setEnabled(b);
        editEmail.setEnabled(b);
        btSend.setEnabled(b);

        if(b == false) {
            btSend.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disable));
        }else{
            btSend.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.menuBackground));
        }
    }


    private void sendMail() {
        loading = ProgressDialog.show(getActivity(), "", "Enviando", true);
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        OkHttpClient client = new OkHttpClient();

        ConfigDAO configDAO = new ConfigDAO(getActivity().getApplicationContext(), null);
        String mailTo = configDAO.getMailPrayer();
        String url = configDAO.getUrlSendMail();
        configDAO.closeConnection();


        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("subject", getString(R.string.prayer_from))
                .add("email", editEmail.getText().toString())
                .add("username", editName.getText().toString())
                .add("to", mailTo)
                .add("suggestion", editPrayer.getText().toString());


        RequestBody formBody = formBuilder.build();


        Request request = new Request.Builder().url(url).post(formBody).build();


        client.newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(final Call call, IOException e) {
                        loading.dismiss();

                        Message.show(getActivity(), getString(R.string.prayer_title), getString(R.string.prayer_error));
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        String res = response.body().string();
                        loading.dismiss();

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                editPrayer.setText("");
                            }
                        });

                        Message.show(getActivity(), getString(R.string.prayer_title), getString(R.string.prayer_ok));
                    }
                });

    }
}
