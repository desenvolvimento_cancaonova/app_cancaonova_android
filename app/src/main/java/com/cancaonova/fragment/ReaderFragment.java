package com.cancaonova.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.cancaonova.DAO.ReaderDAO;
import com.cancaonova.DAO.TimelineDAO;
import com.cancaonova.R;
import com.cancaonova.adapter.AdapterBase;
import com.cancaonova.adapter.ReaderAdapter;
import com.cancaonova.lib.couch.model.Post;

import java.util.ArrayList;


public class ReaderFragment extends Fragment implements AdapterBase.AdapterListener {

    private View view;
    private ListView listView;
    private View msgWithout;
    private ProgressBar loading;
    private ArrayList<Post> posts;
    private ReaderAdapter listAdapter;

    private int openPosition = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.view = inflater.inflate(R.layout.fragment_reader, container, false);
        listView = (ListView) this.view.findViewById(R.id.list_reader);
        msgWithout =  this.view.findViewById(R.id.msg_without);
        loading = (ProgressBar) this.view.findViewById(R.id.loading);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getReader();
            }
        }, 1000);

        return this.view;
    }


    private void getReader(){

        ReaderDAO readerDAO = new ReaderDAO(getActivity());
        posts = readerDAO.getReader();

        if (posts.size() > 0) {
            msgWithout.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            listAdapter = new ReaderAdapter(getActivity(), posts, ReaderFragment.this);
            listView.setAdapter(listAdapter);
            listView.setSelection(0);
        }else{
            msgWithout.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }

        readerDAO.closeConnection();
        this.loading.setVisibility(View.GONE);
    }


    @Override
    public void onResume() {
        super.onResume();
        itemDataChange(openPosition);
        openPosition = -1;
    }

    @Override
    public void onItemDisplayChange(int position) {

    }

    @Override
    public void onItemDataChange(int position) {
        itemDataChange(position);
    }


    @Override
    public void onItemOpen(int position) {
        this.openPosition = position;
    }

    @Override
    public void onItemLikeChange(final int position) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                itemDataChange(position);
            }});
    }


    private void itemDataChange(int position){
        View convertView = listView.getChildAt(position - listView.getFirstVisiblePosition());

        if (convertView == null) {
            return;
        }

        TimelineDAO timelineDAO = new TimelineDAO(getActivity());
        Post p = timelineDAO.get(posts.get(position).get_id());

        listAdapter.getListPosts().set(position, p);
        listAdapter.populateData(position, convertView);

    }
}
