package com.cancaonova.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.cancaonova.BuildConfig;
import com.cancaonova.R;
import com.cancaonova.model.About;
import com.cancaonova.adapter.AboutAdapter;

import java.util.ArrayList;


public class AboutFragment extends Fragment {

    View view;
    ListView listaAbout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.view = inflater.inflate(R.layout.fragment_about, container, false);
        listaAbout = (ListView) this.view.findViewById(R.id.list_about);

        TextView txtVersion = (TextView) this.view.findViewById(R.id.txt_about);

        txtVersion.setText(getActivity().getString(R.string.application)
                        + " " + getActivity().getString(R.string.app_name)
                        + " " + getActivity().getString(R.string.version)
                        + " " + BuildConfig.VERSION_NAME
                        + " " + getActivity().getString(R.string.copyright)
                        + " " + getActivity().getString(R.string.app_name)
                );

        ArrayList<About> abouts = new ArrayList<About>();
        abouts.add(new About("Avalie o App"));
        abouts.add(new About("Sugestões"));
        abouts.add(new About("Termos de Uso"));
        abouts.add(new About("Nossos Aplicativos"));
        abouts.add(new About("Notificação", "Desative essa opção para\nnão receber mais notificações."));

        listaAbout.setAdapter( new AboutAdapter(getActivity(), abouts));

        return this.view;
    }
 }
